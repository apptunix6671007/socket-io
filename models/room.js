 
const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
  users: [mongoose.Schema.Types.ObjectId],
 
});

const Room = mongoose.model('Room', roomSchema);

module.exports = Room;
