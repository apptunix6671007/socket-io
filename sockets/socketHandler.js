// sockets/socketHandler.js
const socketIo = require('socket.io');
const Message = require('../models/message');
const Room = require('../models/room');

module.exports = function (server) {
  const io = socketIo(server);

  io.on('connection', (socket) => {
    console.log('User connected');

    socket.on('joinRoom', async (roomData) => {
      try {
        const room = await Room.findOneAndUpdate(
          { users: { $all: roomData.users } },
          { $setOnInsert: { users: roomData.users } },
          { upsert: true, new: true }
        );
        socket.join(room._id.toString());
        console.log(`User joined room: ${room._id}`);
      } catch (error) {
        console.error(error);
      }
    });

    socket.on('leaveRoom', (roomId) => {
      socket.leave(roomId);
      console.log(`User left room: ${roomId}`);
    });

    socket.on('sendMessage', async (messageData) => {
      try {
        const message = new Message(messageData);
        await message.save();
        io.to(messageData.roomId).emit('message', messageData);
      } catch (error) {
        console.error(error);
      }
    });

    socket.on('disconnect', () => {
      console.log('User disconnected');
    });
  });
};
