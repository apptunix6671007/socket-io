 
const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
  senderId: mongoose.Schema.Types.ObjectId,
  receiverId: mongoose.Schema.Types.ObjectId,
  message: String,
 
});

const Message = mongoose.model('Message', messageSchema);

module.exports = Message;
