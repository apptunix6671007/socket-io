 
const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const socketHandler = require('./sockets/socketHandler');
const messageRoutes = require('./routes/messageRoutes');

const app = express();
const server = http.createServer(app);
const PORT = process.env.PORT || 3000;

mongoose.connect('mongodb://localhost:27017/chatApp', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
});

app.use(express.json());
app.use('/messages', messageRoutes);

socketHandler(server);

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
